const items = require("./3-arrays-vitamins.cjs")
function ItemsWithVitaminC(items) {
    let arr = []
    if (!Array.isArray(items)) {
        return arr
    } else {
        const FilteredItemsWithVitaminC = items.filter(data => {
            return data.contains.includes("C")
        })
        return FilteredItemsWithVitaminC
    }
}
console.log(ItemsWithVitaminC(items))