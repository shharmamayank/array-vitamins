const items = require("./3-arrays-vitamins.cjs")
function getAllItemsAvailable(items) {
    let arr = []
    if (!Array.isArray(items)) {
        return arr
    } else {
        const AllItemsAvailable = items.filter(data => {
            return data.available
        })
        return AllItemsAvailable
    }
}

console.log(getAllItemsAvailable(items))