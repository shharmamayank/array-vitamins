const items = require("./3-arrays-vitamins.cjs")
function sortItemBasedOnVitamins(items) {
    let arr = []
    if (!Array.isArray(items)) {
        return arr
    } else {
        const sortedVitamins = items.sort((vitamin1, vitamin2) => {
            let vitaminCount = vitamin1.contains.substring(",").length;
            let vitaminCount2 = vitamin2.contains.substring(",").length
            if (vitaminCount > vitaminCount2) {
                return -1
            } else if (vitaminCount > vitaminCount2) {
                return 1
            }
            return 0
        })
        return sortedVitamins
    }
}
console.log(sortItemBasedOnVitamins(items))