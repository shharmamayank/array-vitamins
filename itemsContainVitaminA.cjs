const items = require("./3-arrays-vitamins.cjs")
function ItemsWithVitaminA(items) {
    let arr = []
    if (!Array.isArray(items)) {
        return arr
    } else {
        const FilteredItemsWithVitaminA = items.filter(data => {
            return data.contains.includes("A")
        })
        return FilteredItemsWithVitaminA
    }
}
console.log(ItemsWithVitaminA(items))